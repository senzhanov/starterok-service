# Spring Boot + Angular 13 + MongoDB

### Repository
`https://gitlab.com/senzhanov/starterok-service`

## Environment
#### 1. Java 15.0.1
#### 2. MongoDB - 4.4.3
#### 3. Maven - 3.2.5
#### 4. Angular CLI: 13.3.5
#### 5. Node: 16.15.0

## Server-side technologies
#### 1. Spring Boot - 2.6.8
#### 2. Java 15.0.1   
#### 3. JDK - 1.8
#### 4. MongoDB - 4.4.3
#### 5. Maven - 3.2.5
#### 6. JWT Authorization
#### 7. mapstruct 1.4.2 mappings between Java entity 
    https://examples.javacodegeeks.com/spring-boot-mapstruct-example/
#### 12. File uploader
    https://www.bezkoder.com/spring-boot-upload-file-database/



# Run local
 - `backend: mvn spring-boot:run`
 - `frontend: ng serve`

# Run on server
- `build *.jar`:`mvn clean package -P prod -X -DskipTests`
- `run`:`java -jar backend/target/backend-0.0.1.jar`

## Run frontend

## Agreements
1.Use ResponseEntity: https://www.baeldung.com/spring-response-entity
 - 200 Ok `{entity}`
 - other status: 
   `{
        "status": 404,
        "statusText": "OK",
        "url": "http://localhost:8080/api/user/language/UA",
        "ok": false,
        "name": "HttpErrorResponse",
        "message": "Http failure response for http://localhost:8080/api/user/language/UA: 404 OK",
        "error": {
            "timestamp": "2022-05-29T13:02:00.238+00:00",
            "message": "UserServiceImpl.getAuthorizedUser(). Not found User by id: 62913814d403244660ee4837",
            "details": "uri=/api/user/language/UA"
        }
        "headers": {
            "normalizedNames": {},
            "lazyUpdate": null
        },
    }`


### MongoDB 
## Create dump
 - `mongodump --db it-generally --out=/Users/alex/Downloads/dump --gzip`
## Restore dump 
 - `mongorestore -d it-generally it-generally --drop --gzip`
