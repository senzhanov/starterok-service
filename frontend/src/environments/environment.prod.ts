export const environment = {
  dev: false,
  test: false,
  prod: true,
  environmentName: 'prod',
  apiUrl: 'http://localhost-prod:8080'
};
