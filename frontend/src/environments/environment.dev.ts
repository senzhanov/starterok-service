export const environment = {
  dev: true,
  test: false,
  prod: false,
  environmentName: 'dev',
  apiUrl: 'http://localhost:8080'
};
