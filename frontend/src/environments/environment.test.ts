export const environment = {
  dev: false,
  test: true,
  prod: false,
  environmentName: 'test',
  apiUrl: 'http://localhost-test:8080'
};
