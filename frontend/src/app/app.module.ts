import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Importing internationalisation translator modules in the application.
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AceModule } from 'ngx-ace-wrapper';
import {ModalModule} from "ngx-bootstrap/modal";
import {CarouselModule} from "ngx-bootstrap/carousel";
import { CodemirrorModule } from "@ctrl/ngx-codemirror";
import { ToastrModule } from 'ngx-toastr';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// "HttpLoaderFactory" returns an object that loads the translations using the .json file.
// Function returns the "TranslateHttpLoader" object for the AOT compiler.

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { EditorModule } from "@tinymce/tinymce-angular";

import {ButtonsModule} from "ngx-bootstrap/buttons";
import {BsDropdownModule} from "ngx-bootstrap/dropdown";
import {Observable} from "rxjs";
import {parse} from 'yaml';
import { map } from 'rxjs/operators';
import {MainComponent} from "./public/main/main.component";
import {RegistrationComponent} from "./public/registration/registration.component";
import {ContactsComponent} from "./public/contacts/contacts.component";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}



class TranslateYamlHttpLoader implements TranslateLoader {
  constructor(
    private http: HttpClient,
    public path: string = '/assets/i18n/'
  ) {}

  public getTranslation(lang: string): Observable<Object> {
    return this.http
      .get(`${this.path}${lang}.yaml`, { responseType: 'text' })
      .pipe(map((data) => parse(data)));
  }
}

@NgModule({
  declarations: [
    AppComponent,

    //Public
    MainComponent,
    RegistrationComponent,
    ContactsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: 'ua',
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateYamlHttpLoader(http),
        deps: [HttpClient],
      },
    }),
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    EditorModule,
    CodemirrorModule,
    CarouselModule.forRoot(), // Carousel [https://valor-software.com/ngx-bootstrap/#/components/carousel?tab=examples]
    ModalModule.forRoot(), // Modal windows https://valor-software.com/ngx-bootstrap/#/components/modals
    AceModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    ButtonsModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
