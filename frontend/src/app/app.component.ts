import {Component, HostListener, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {BsDropdownDirective} from "ngx-bootstrap/dropdown";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {ToastrService} from "ngx-toastr";

interface Language {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private roles: string[] = [];
  isLoggedIn = false;
  isShowNavbarCourses = false;
  showStudentBoard = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  showTeacherBoard = false;
  username?: string;

  scrollY: number = 0;
  isHide: boolean;
  currentLang: string = 'ua';

  modalRef?: BsModalRef;
  config = {
    animated: true,
    // class: 'modal-sm'
    class: 'modal-lg'
  };

  languages: Language[] = [
    {value: 'ua', viewValue: 'UA'},
    {value: 'en', viewValue: 'EN'},
    {value: 'ru', viewValue: 'RU'},
  ];

  @ViewChild('dropdown') dropdown: BsDropdownDirective;

  constructor(private translate: TranslateService, private modalService: BsModalService,
              private toastr: ToastrService) {
    let langStored = localStorage.getItem("lang");
    if (langStored === null || langStored === undefined) {
      localStorage.setItem("lang", "ua");
      translate.use('ua');
      this.currentLang = 'ua';
    } else {
      translate.use(langStored);
      this.currentLang = langStored;
    }
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    // this.dropdown.show();
    // console.log(document.getElementById("dropdown-basic"));
    // this.publicService.getAllJournalCourses().subscribe(
    //   data => {
    //     this.courses = data;
    //     var list = document.getElementById("dropdown-basic");
    //     this.removeAllChildNodes(list);
    //     for (const course of this.courses) {
    //       //<li role="menuitem"><a class="dropdown-item" href="#">Separated link</a></li>
    //       var li = document.createElement("li");
    //       li.classList.add("menuitem");
    //       var link = document.createElement("a");
    //       link.classList.add("dropdown-item");
    //       var text;
    //       if (this.currentLang === "ua") {
    //          text = document.createTextNode(course.nameUa);
    //       }
    //       if (this.currentLang === "en") {
    //          text = document.createTextNode(course.nameEn);
    //       }
    //       if (this.currentLang === "ru") {
    //          text = document.createTextNode(course.nameRu);
    //       }
    //       link.appendChild(text);
    //       link.href = "/courses/" + course.id;
    //       li.appendChild(link);
    //       list.appendChild(li);
    //     }
    //     this.dropdown.hide();
    //   },
    //   err => {console.log('Error while getting JournalCourses: ', err);}
    // );
  }

  showHideNavbarCourses() {
    this.isShowNavbarCourses = !this.isShowNavbarCourses;
  }
  hideNavbarCourses() {
    this.isShowNavbarCourses = false;
  }

  removeAllChildNodes(parent) {
    while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
    }
  }

  logout(): void {
    window.location.reload();
  }

  switchLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem("lang", language);
    this.currentLang = language;
    window.location.reload();
  }

  hideMenuFunction() {
    this.isHide = true;
  }

  showMenuFunction() {
    this.isHide = false;
  }

  @HostListener('window:scroll', []) onScroll(): void {
    /* Hide header when scroll more than */
    if (window.scrollY > 500) {
      this.hideNavbarCourses()
    } else {
      // this.isHide = false;
    }
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.config);
    this.hideNavbarCourses();
  }
  closeModal() {
    this.modalRef?.hide();
  }

  signup() {
  }
}
