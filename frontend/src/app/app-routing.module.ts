import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {MainComponent} from "./public/main/main.component";
import {RegistrationComponent} from "./public/registration/registration.component";
import {ContactsComponent} from "./public/contacts/contacts.component";

const routes: Routes = [
  //Public
  { path: 'main', component: MainComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: 'contacts', component: ContactsComponent },

  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: '**', redirectTo: 'main', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
