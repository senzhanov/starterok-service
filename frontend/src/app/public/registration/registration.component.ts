import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  content?: string;
  currentLang: string;

  constructor() { }

  ngOnInit(): void {
    let langStored = localStorage.getItem("lang");
    if (langStored === null || langStored === undefined) {
      this.currentLang = 'ua';
    } else {
      this.currentLang = langStored;
    }
  }
}
