import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  content?: string;

  indexLeftSlide = 0;
  indexActiveSlide = 0;
  indexRightSlide = 0;
  isAnimated = true;

  currentLang: string;

  constructor() { }

  ngOnInit(): void {
    let langStored = localStorage.getItem("lang");
    if (langStored === null || langStored === undefined) {
      this.currentLang = 'ua';
    } else {
      this.currentLang = langStored;
    }

    // this.publicService.getPublicContent().subscribe(
    //   data => {
    //     this.content = data;
    //   },
    //   err => {
    //     this.content = JSON.parse(err.error).message;
    //   }
    // );
    // this.publicService.getAllJournalCourses().subscribe(
    //   data => {
    //       this.courses = data;
    //       console.log("this.courses -> ", this.courses)
    //       this.slides = [];
    //       for (const course of this.courses) {
    //         this.slides.push({
    //             image: course.previewFile.url,
    //             title: this.getCourseName(course),
    //             text: this.getCourseDesc(course),
    //             alt: this.getCourseName(course),
    //             link: '/courses/' + course.id
    //           });
    //       }
    //     },
    //   err => {console.log('Error while getting JournalCourses: ', err);}
    // );
  }


  activeSlideChange(event: number) {
  }
}
