import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  content?: string;
  currentLang: string;

  constructor() { }

  ngOnInit(): void {
    let langStored = localStorage.getItem("lang");
    if (langStored === null || langStored === undefined) {
      this.currentLang = 'ua';
    } else {
      this.currentLang = langStored;
    }
  }
}
