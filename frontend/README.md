# All Components
[ngx-bootstrap](https://valor-software.com/ngx-bootstrap/#/components) version 9.0.0.

# AngularSpringbootClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# ------------------------------------------
### Install the latest version of Angular CLI
npm install -g @angular/cli

### Integrate JQuery and Bootstrap with Angula
npm install bootstrap jquery --save

### Enable CORS on Server
To enable CORS on the server, add a
@CrossOrigin annotation to the UserController:

### Running Angular App
1. ng serve
2. or: ng serve --port 4201

## Front end technologies
#### 1. Angular CLI: 13.3.5
#### 2. Bootstrap CSS 4.x.x
#### 3. Package Manager: npm 8.5.5
#### 4. Node: 16.15.0
#### 5. JQuery
#### 6. Translate ngx-translate
    https://www.javacodegeeks.com/2019/05/angular-internationalization-example.html
#### 7. Alerts - ngx-toastr
    https://www.npmjs.com/package/ngx-toastr
#### 8. Editor for text - tinymce
    https://www.azitmentor.hu/blog/how_to_use_tinymce_with_angular_13
#### 9. Redactor for code - ACE
    https://medium.com/@ofir3322/create-an-online-ide-with-angular-6-nodejs-part-1-163a939a7929
    https://stackoverflow.com/questions/69691071/how-can-i-use-ace-editor-in-angular
    https://allenhwkim.medium.com/angular-build-code-editor-with-ace-2cec03b143fd
    https://blog.shhdharmen.me/how-to-setup-ace-editor-in-angular
    Options - https://ace.c9.io/#nav=howto
#### 10. Confirm Window
    https://valor-software.com/ngx-bootstrap/#/components/modals?tab=overview
#### 11. ngx-bootstrap 8.0.0
    (All tools - https://bestprogrammer.ru/izuchenie/10-luchshih-bibliotek-angular-dlya-veb-razrabotchikov)
    https://valor-software.com/ngx-bootstrap/#/
    https://github.com/valor-software/ngx-bootstrap
#### 12. File uploader
    https://www.bezkoder.com/angular-13-spring-boot-file-upload/
#### 13. ICONS
    https://icons.getbootstrap.com/
#### 14. Placement
    https://css-tricks.com/exploring-css-grids-implicit-grid-and-auto-placement-powers/
    https://itchief.ru/html-and-css/positioning-elements
    https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Auto-placement_in_CSS_Grid_Layout
#### 15. Dynamic Tables
    http://thenewcode.com/327/CSS-Dynamic-Customized-Tables
#### 16. Валидация форм
    https://angdev.ru/doc/forms-validation/
