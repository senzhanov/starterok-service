package net.guides.springboot2.springboot2jpacrudexample;

import com.starterok.service.Application;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
@ExtendWith(MockitoExtension.class)
public class ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
