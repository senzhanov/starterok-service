package com.starterok.service.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;

@Data
public class BaseDocument {

    @Id
    private String id;

    private LocalDateTime lastUpdateDate = LocalDateTime.now();

    private LocalDateTime createdDate = LocalDateTime.now();
}
