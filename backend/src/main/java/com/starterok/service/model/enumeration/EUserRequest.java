package com.starterok.service.model.enumeration;

public enum EUserRequest {

    NEW,
    IN_PROGRESS,
    PENDING,
    DONE
}
