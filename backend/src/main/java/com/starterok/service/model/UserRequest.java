package com.starterok.service.model;

import com.starterok.service.model.enumeration.EUserRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "user_request")
public class UserRequest extends BaseDocument {

    private String firstName;

    private String lastName;

    private String email;

    private String phone;

    private EUserRequest status = EUserRequest.NEW;

    private String comment;
}
