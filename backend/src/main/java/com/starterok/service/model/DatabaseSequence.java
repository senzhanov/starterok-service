package com.starterok.service.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document(collection = "database_sequences")
public class DatabaseSequence extends BaseDocument {

    private long seq;
}
