package com.starterok.service.model.enumeration;

public enum ELang {
    UA,
    EN,
    RU
}
