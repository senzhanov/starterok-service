package com.starterok.service.model.enumeration;

public enum ERole {
    ROLE_STUDENT,
    ROLE_TEACHER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
