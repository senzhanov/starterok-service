package com.starterok.service.model.enumeration;

import java.util.ArrayList;
import java.util.List;

public enum EAccountRole {

    STUDENT,
    TEACHER,
    ADMIN,
    DIRECTOR;

    public static List<EAccountRole> list() {
        List<EAccountRole> list = new ArrayList<>();
        for (EAccountRole items : EAccountRole.values()) {
            list.add(items);
        }
        return list;
    }
}
