package com.starterok.service.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.starterok.service.model.enumeration.ELang;
import com.starterok.service.model.enumeration.ERole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document (collection = "user")
public class User extends BaseDocument {

	@NotBlank @NotEmpty @NotNull
    @Size(max=100, message = "Message")
	private String firstName;

	@NotBlank @NotEmpty @NotNull
	@Size(max=100, message = "Message")
	private String lastName;

	private String password;

	@NotBlank
    @Size(max=100, message = "Message")
	@Indexed(unique = true)
	private String email;
	private ELang lang = ELang.EN;
	private Boolean isValidEmail = false;
	private String phone;
	private String telegram;

	private Set<ERole> roles = new HashSet<>();

	private String correspondenceId;

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ "]";
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public User(String email, String password, String firstName, String lastName, String phone, Set<ERole> roles) {
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
		this.roles = roles;
	}
}
