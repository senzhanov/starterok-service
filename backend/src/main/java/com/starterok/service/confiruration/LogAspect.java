package com.starterok.service.confiruration;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class LogAspect {


//    @Around("execution(public * *(..)) " +
//            "&& within(com.it.generally..*) " +
//            "&& !execution(* ua.varus.mobile.app.scheduling.ScheduleSynchronizer.*(..))" +
//            "&& !execution(* ua.varus.mobile.app.service.OtpMonitoring.*(..))")

    @Around("execution(public * *(..)) && within(com.it.generally..*) ")
    private Object log(ProceedingJoinPoint pjp) throws Throwable {
        String classWithMethodName = pjp.getSignature().toShortString();

        log.info("Call: {}", classWithMethodName);
        Object processedResult = pjp.proceed();
        log.info("Processed: {}", classWithMethodName);

        return processedResult;
    }

}
