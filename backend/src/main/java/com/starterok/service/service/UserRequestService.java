package com.starterok.service.service;

import com.starterok.service.dto.user.UserRequestDTO;
import com.starterok.service.model.UserRequest;
import com.starterok.service.model.enumeration.EUserRequest;

import java.util.List;

public interface UserRequestService {

    UserRequest saveUserRequest(UserRequest userRequest);

    List<UserRequestDTO> getUserRequestsByStatus(EUserRequest eUserRequest);
}
