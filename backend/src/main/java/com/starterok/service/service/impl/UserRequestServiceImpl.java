package com.starterok.service.service.impl;

import com.starterok.service.dto.user.UserRequestDTO;
import com.starterok.service.model.UserRequest;
import com.starterok.service.model.enumeration.EUserRequest;
import com.starterok.service.repository.UserRequestRepository;
import com.starterok.service.service.UserRequestService;
import com.starterok.service.service.mapper.UserRequestMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserRequestServiceImpl implements UserRequestService {

    private final UserRequestRepository userRequestRepository;
    private final UserRequestMapper userRequestMapper;

    public UserRequestServiceImpl (UserRequestRepository userRequestRepository, UserRequestMapper userRequestMapper){
        this.userRequestRepository = userRequestRepository;
        this.userRequestMapper = userRequestMapper;
    }

    @Override
    public UserRequest saveUserRequest(UserRequest userRequest) {
        userRequest.setLastUpdateDate(LocalDateTime.now());
        return userRequestRepository.save(userRequest);
    }

    @Override
    public List<UserRequestDTO> getUserRequestsByStatus(EUserRequest eUserRequest) {
        return userRequestMapper.toDtoList(userRequestRepository.findUserRequestsByStatus(eUserRequest));
    }
}
