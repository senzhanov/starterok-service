package com.starterok.service.service;

import com.starterok.service.exception.EntityNotFoundException;
import com.starterok.service.model.User;

public interface UserService {

    User getAuthorizedUser() throws EntityNotFoundException;

    Boolean existsByEmail(String email) throws EntityNotFoundException;

    User save(User user);
}
