package com.starterok.service.service.impl;

import com.starterok.service.confiruration.security.services.UserDetailsImpl;
import com.starterok.service.exception.EntityNotFoundException;
import com.starterok.service.model.User;
import com.starterok.service.repository.UserRepository;
import com.starterok.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public User getAuthorizedUser() throws EntityNotFoundException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        Optional<User> optionalUser = userRepository.findById(userDetails.getId());
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }
        throw new EntityNotFoundException("UserServiceImpl.getAuthorizedUser(). Not found User by id: "
                + userDetails.getId());
    }

    @Override
    public Boolean existsByEmail(String email) throws EntityNotFoundException {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User save(User user) {
        user.setLastUpdateDate(LocalDateTime.now());
        return userRepository.save(user);
    }
}
