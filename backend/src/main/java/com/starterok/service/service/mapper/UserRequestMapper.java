package com.starterok.service.service.mapper;

import com.starterok.service.dto.user.UserRequestDTO;
import com.starterok.service.model.UserRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface UserRequestMapper extends EntityMapper<UserRequestDTO, UserRequest>  {
}
