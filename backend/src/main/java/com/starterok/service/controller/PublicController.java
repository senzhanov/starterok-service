package com.starterok.service.controller;

import com.starterok.service.dto.user.UserRequestDTO;
import com.starterok.service.model.UserRequest;
import com.starterok.service.service.UserRequestService;
import com.starterok.service.service.mapper.UserRequestMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/public")
public class PublicController {

    private final UserRequestService userRequestService;
    private final UserRequestMapper userRequestMapper;

    public PublicController(UserRequestService userRequestService, UserRequestMapper userRequestMapper) {
        this.userRequestService = userRequestService;
        this.userRequestMapper = userRequestMapper;

    }

    @GetMapping("/get")
    public ResponseEntity<?> allAccess() {
        return new ResponseEntity("Контент", HttpStatus.OK);
    }


    @PostMapping("/user-request")
    public ResponseEntity<UserRequestDTO> createUserRequest(@RequestBody UserRequestDTO userRequestDTO) {
        UserRequest userRequest = userRequestMapper.toEntity(userRequestDTO);
        UserRequest userRequestSaved = userRequestService.saveUserRequest(userRequest);
        return new ResponseEntity(userRequestMapper.toDto(userRequestSaved), HttpStatus.OK);
    }
}
