package com.starterok.service.repository;

import com.starterok.service.model.UserRequest;
import com.starterok.service.model.enumeration.EUserRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UserRequestRepository extends MongoRepository<UserRequest, String> {

    List<UserRequest> findUserRequestsByStatus(EUserRequest status);
}
