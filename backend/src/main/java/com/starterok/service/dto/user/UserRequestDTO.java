package com.starterok.service.dto.user;

import com.starterok.service.model.enumeration.EUserRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDTO {

    private String id;

    private String firstName = "";

    private String lastName = "";

    private String email = "";

    private String phone = "";

    private EUserRequest status = EUserRequest.NEW;

    private String comment = "";
}
